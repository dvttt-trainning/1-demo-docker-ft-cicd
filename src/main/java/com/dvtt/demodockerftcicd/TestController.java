package com.dvtt.demodockerftcicd;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by linhtn on 9/18/2021.
 */
@RestController
@RequestMapping("/test")
@AllArgsConstructor
@Slf4j
public class TestController {

    private final UserService userService;
    private final StringRedisTemplate redisTemplate;

    @GetMapping("/user/{id}")
    public String getUserById(@PathVariable String id) {
        String key = String.format("%s_%s", "userDetail", id);

        Boolean hasKey = redisTemplate.hasKey(key);
        if (null != hasKey && hasKey) {

            log.info("------get user from redis --------");
            return redisTemplate.opsForValue().get(key);
        }
        String userById = userService.findUserById(id);
        redisTemplate.opsForValue().set(key, userById);
        return userById;
    }
}
