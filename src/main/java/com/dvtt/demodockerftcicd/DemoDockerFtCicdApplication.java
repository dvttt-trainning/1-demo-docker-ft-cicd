package com.dvtt.demodockerftcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDockerFtCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDockerFtCicdApplication.class, args);
	}

}
