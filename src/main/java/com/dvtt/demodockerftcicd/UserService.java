package com.dvtt.demodockerftcicd;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by linhtn on 9/18/2021.
 */
@Service
@Slf4j
public class UserService {

    public String findUserById(String id) {
        String url
                = "https://reqres.in/api/users/" + id;
        log.info("------get user from api--------");
        log.info("------url: {}", url);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForEntity(url, String.class).getBody();
    }
}
