FROM eclipse-temurin:11.0.13_8-jre-alpine
ARG PACKAGE_NAME=demo-docker-ft-cicd
#adding a non-root user and group to the container’s system
RUN addgroup -g 2000 -S dvtt && adduser -u 2000 -S dvtt -G dvtt -h /home/dvtt
#setting our root directory because we cannot put contents on root now
WORKDIR /home/dvtt
#copying executable jar there from target folder
COPY target/$PACKAGE_NAME*.jar $PACKAGE_NAME.jar
#changing owner and permissions of that jar
RUN chown dvtt:dvtt $PACKAGE_NAME.jar && chmod 700 $PACKAGE_NAME.jar
#specifying newly created user
USER dvtt
ENTRYPOINT java -Xms256m -Xmx2G -jar demo-docker-ft-cicd.jar