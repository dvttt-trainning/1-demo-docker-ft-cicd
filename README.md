
# Getting Started

- execute `mvn clean compile -DskipTests=true package` to compile and packing jar file

- execute `docker build -t demo-java-dockerfile:1.0.9 .` to build image
  - **1.0.9** is version, you can put anything else

- execute `docker run -e ENV=dev -d --name demo-java-dockerfile-c -p 8086:8080 demo-java-dockerfile:1.0.9` to start a container
  - **8090** is port that you want to expose to host machine
  - **demo-reactjs-dockerfile** is name of image that you are created a few minutes ago
  - **ENV** is environment that you want to build: dev, test, staging, prod

- execute `docker logs demo-java-dockerfile-c` to show logs of container
- execute `docker exec -it demo-java-dockerfile-c sh` to exec into container
